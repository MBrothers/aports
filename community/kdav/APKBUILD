# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdav
pkgver=5.72.0
pkgrel=0
pkgdesc="A DAV protocol implementation with KJobs"
url="https://community.kde.org/Frameworks"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
license="LGPL-2.0-or-later"
depends_dev="qt5-qtbase-dev qt5-qtxmlpatterns-dev kcoreaddons-dev kio-dev ki18n-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdav-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="3fdff10fa072a95c39fc4cc2ce7fa2b713f5b9b19d04afd44ecaf9553be4a183497ab3de5eff85c833f5b0e134a38ca227f0bb9f5e35711d6eaa091db734dc18  kdav-5.72.0.tar.xz"
